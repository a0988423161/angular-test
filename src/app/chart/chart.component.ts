import { Component, Input, ViewChild } from '@angular/core';
import {
  ApexNonAxisChartSeries,
  ApexChart,
  ApexFill,
  ApexPlotOptions,
  ApexStroke,
  ChartComponent as _ChartComponent,
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  labels: string[];
  plotOptions: ApexPlotOptions;
  fill: ApexFill;
  stroke: ApexStroke;
};

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrl: './chart.component.scss',
})
export class ChartComponent{
  // @ViewChild('chart') chart: _ChartComponent;
  @Input() title: string;
  @Input() value: number;
  @Input() max: number;

  public chartOptions: Partial<ChartOptions>;


  constructor() {
    this.title = ''
    this.value = 0;
    this.max = 0;

    this.chartOptions = {
      series: [this.value/this.max*100],
      chart: {
        height: 300,
        type: 'radialBar',
        toolbar: {
          show: false,
        },
      },
      plotOptions: {
        radialBar: {

          inverseOrder: false,
          startAngle: 0,
          endAngle: 360,
          hollow: {
            margin: 0,
            size: '70%',
            background: 'transparent',
            image: undefined,
            position: 'front',
            // dropShadow: {
            //   enabled: true,
            //   top: 3,
            //   left: 0,
            //   blur: 4,
            //   opacity: 0.24,
            // },
          },
          track: {
            background: '#fff',
            opacity: 0.2,
            strokeWidth: '100%',
            margin: 0, // margin is in pixels
            dropShadow: {
              enabled: true,
              top: -3,
              left: 0,
              blur: 4,
              opacity: 0.35,
            },
          },
          dataLabels: {
            show: true,
            name: {
              offsetY: 40,
              show: true,
              color: '#7B7B83',
              fontSize: '17px',
              fontWeight: 400,
            },
            value: {
              formatter: function (val) {
                return parseInt(val.toString(), 10).toString() + '%';
              },
              color: '#F5F6F9',
              fontSize: '48px',
              show: true,
              offsetY: -8,
            },
          },
        },
      },
      fill: {
        type: 'gradient',
        gradient: {
          shade: 'dark',
          type: 'horizontal',
          shadeIntensity: 0.5,
          inverseColors: true,
          opacityFrom: 1,
          opacityTo: 1,
          colorStops: [
            [
              {
                offset: 20,
                color: '#EC2E88',
                opacity: 1,
              },
              {
                offset: 100,
                color: '#984BE4',
                opacity: 1,
              },
            ],
          ],
        },
      },
      stroke: { lineCap: 'round' },
      labels: [this.title],
    };
  }
}
