import { Component } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent{
  kpis = [
    {
      title: '工作總時數',
      value: 1430,
      max: 2000,
      unit: '$'
    },
    {
      title: '跑步里程',
      value: 100,
      max: 200,
      unit: ''
    }
  ]
}
