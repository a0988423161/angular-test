import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeekArrangeComponent } from './week-arrange.component';

describe('WeekArrangeComponent', () => {
  let component: WeekArrangeComponent;
  let fixture: ComponentFixture<WeekArrangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WeekArrangeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(WeekArrangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
